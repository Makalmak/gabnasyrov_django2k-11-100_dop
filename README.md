Команды для развертывания репозитория
 - python -m venv .venv - создание виртуального окружения
 - source ./.venv/bin/activate - вход в виртуальное окружение на Linux
 - .venv\Scripts\activate - вход в виртуальное окружение на Windows
 - pip install -r requirements.txt - установка зависимостей
 - python manage.py migrate - запуск миграций
 - python manage.py runserver - запуск приложения
