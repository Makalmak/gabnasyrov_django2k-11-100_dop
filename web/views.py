from django.shortcuts import render
from .models import Site
from .forms import LinkForm
from bs4 import BeautifulSoup
import requests
import pyshorteners


def main_view(request):
    sites = Site.objects.all()
    if request.method == 'POST':
        form = LinkForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data.get('link')
            page = requests.get(url)
            soup = BeautifulSoup(page.text, "html.parser")
            title = soup.find('head').find('title').text

            short = pyshorteners.Shortener().tinyurl.short(url)

            site = Site()
            site.title = title
            site.shortlink = short
            site.save()
    else:
        form = LinkForm()
        short = ''

    return render(request, 'web/main.html', {
        'sites': sites,
        'form': form,
        'short': short
    })