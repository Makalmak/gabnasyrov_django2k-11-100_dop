from django.db import models


class Site(models.Model):
    title = models.CharField(max_length=500)
    shortlink = models.CharField(max_length=500)
    cnt_jumps = models.IntegerField(default=0)

